package com.cloudconnective.tutorial_layouts.listeners;

import android.app.Activity;
import android.view.View;
import android.widget.Toast;

import com.cloudconnective.tutorial_layouts.R;

/**
 * Created by Malik A. Abualzait on 11/14/17.
 */

public class ClickHandler implements View.OnClickListener {
    private Activity activity;
    public  ClickHandler(Activity activity){
        this.activity=activity;
    }
    @Override
    public void onClick(View v) {
         Toast.makeText(activity, R.string.notification,Toast.LENGTH_LONG).show();

    }
}
