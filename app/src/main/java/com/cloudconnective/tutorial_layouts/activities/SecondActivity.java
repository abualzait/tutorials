package com.cloudconnective.tutorial_layouts.activities;

import android.app.*;
import android.content.Intent;
import android.os.*;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.cloudconnective.tutorial_layouts.R;
import com.cloudconnective.tutorial_layouts.models.Student;

/**
 * Created by Malik A. Abualzait on 11/15/17.
 */

public class SecondActivity extends Activity {

    private Student reveivedStudent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Intent i = getIntent();
        reveivedStudent = (Student) i.getSerializableExtra("student");
        if (reveivedStudent != null) {

        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String myName = bundle.getString("name");
            TextView myNameTextView = findViewById(R.id.result);
            myNameTextView.setText(myName);
        }
        new GetApiData().execute();
    }

    private class GetApiData extends AsyncTask<Integer, Void, Boolean> {
        private ProgressDialog dialog;



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(SecondActivity.this);
            dialog.setTitle("Loading...");
            dialog.setMessage("Please wai....");
            dialog.show();
        }


        @Override
        protected Boolean doInBackground(Integer... integers) {
            return null;
        }

        @Override
        protected void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
            dialog.dismiss();
        }
    }
}
