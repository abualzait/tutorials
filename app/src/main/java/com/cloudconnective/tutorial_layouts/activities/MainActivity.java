package com.cloudconnective.tutorial_layouts.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.cloudconnective.tutorial_layouts.R;
import com.cloudconnective.tutorial_layouts.SqlLite.DBHelper;
import com.cloudconnective.tutorial_layouts.models.Student;

public class MainActivity extends Activity{

    private static String TAG = "DB";
    DBHelper mydb ;
     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mydb = new DBHelper(MainActivity.this);
        insertValue();
     }

    private void insertValue() {
     mydb.insertContact(new Student();
        Log.i(TAG, "student A inserted....");
     }

    public  void goMethod(View view) {

         Student student = new Student(1,"Malik","22");
         Intent intent = new Intent(MainActivity.this,SecondActivity.class);
         intent.putExtra("id",1);
         intent.putExtra("name","Heba");
         intent.putExtra("student",student);
         startActivity(intent);
     }
}
