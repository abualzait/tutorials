package com.cloudconnective.tutorial_layouts.SqlLite;

import android.content.*;
import android.database.*;
import android.database.sqlite.*;

import com.cloudconnective.tutorial_layouts.models.Student;

import java.util.ArrayList;

/**
 * Created by Malik A. Abualzait on 11/16/17.
 */

public class DBHelper extends SQLiteOpenHelper {
    //Constants
    private final static String DB_NAME="testDB";
    private final static Integer DB_VERSION=1 ;

    public static final String CONTACTS_TABLE_NAME = "contacts";
    public static final String CONTACTS_COLUMN_ID = "id";
    public static final String CONTACTS_COLUMN_NAME = "name";
    public static final String CONTACTS_COLUMN_EMAIL = "email";
    public static final String CONTACTS_COLUMN_STREET = "street";
    public static final String CONTACTS_COLUMN_CITY = "place";
    public static final String CONTACTS_COLUMN_PHONE = "phone";


    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table "+CONTACTS_TABLE_NAME+
                        "("+CONTACTS_COLUMN_ID +" integer primary key," +
                        " "+CONTACTS_COLUMN_NAME+" text," +
                        " "+CONTACTS_COLUMN_PHONE+" text," +
                        " "+CONTACTS_COLUMN_EMAIL+" text," +
                        " "+CONTACTS_COLUMN_STREET+" text," +
                        " "+CONTACTS_COLUMN_CITY+" text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+CONTACTS_TABLE_NAME);
        onCreate(db);

    }
    //CRUD Operations
    public boolean insertContact (String name, String phone, String email, String street,String place) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CONTACTS_TABLE_NAME, name);
        contentValues.put("phone", phone);
        contentValues.put("email", email);
        contentValues.put("street", street);
        contentValues.put("place", place);
        db.insert("contacts", null, contentValues);
        return true;
    }

    public Cursor getContactById(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from contacts where id="+id+"", null );
        return res;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, CONTACTS_TABLE_NAME);
        return numRows;
    }

    public boolean updateContact (Integer id, String name, String phone, String email, String street,String place) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("phone", phone);
        contentValues.put("email", email);
        contentValues.put("street", street);
        contentValues.put("place", place);
        db.update("contacts", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public Integer deleteContact (Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("contacts",
                "id = ? ",
                new String[] { Integer.toString(id) });
    }

    public ArrayList<String> getAllCotacts() {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from contacts", null );


        if(res.moveToFirst()){
            do {
                array_list.add(res.getString(res.getColumnIndex(CONTACTS_COLUMN_NAME)));
                String email = res.getString(res.getColumnIndex(CONTACTS_COLUMN_EMAIL));
                res.moveToNext();
            }while (res.moveToNext()!=false);

        }
        return array_list;
    }

    public void insertContact(Student student) {
    }
}

